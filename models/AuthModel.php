<?php

namespace app\models;

use app\common\Entities\Social;
use app\common\Interfaces\SocialInterface;
use app\common\Repositories\CustomerRepository;
use app\common\Repositories\SocialRepository;
use app\common\Validators\SocialValidator;
use yii\base\Model;

/**
 * Class AuthModel
 *
 * @package app\models
 */
class AuthModel extends Model implements SocialInterface
{
	/** @var  string */
	public $id;

	/** @var  string */
	public $name;

	/** @var  string */
	public $social_data;

	/** @var  string */
	public $social;

	/** @var  string */
	public $email;

	/** @var SocialInterface */
	public $socialEntity;

	/** @var SocialRepository */
	protected $repository;

	/** @var CustomerRepository */
	protected $customerRepository;

	/** @var  int */
	protected $customer_id;

	/**
	 * AuthModel constructor.
	 *
	 * @param \app\common\Repositories\SocialRepository $repository
	 * @param \app\common\Repositories\CustomerRepository $customerRepository
	 */
	public function __construct(SocialRepository $repository, CustomerRepository $customerRepository)
	{
		parent::__construct();
		$this->repository = $repository;
		$this->customerRepository = $customerRepository;
	}

	//TODO this validation for  facebook, if need  more, must  add scenarios

	/**
	 * @return array
	 */
	public function rules()
	{
		return [
			[['id', 'social', 'social_data', 'email', 'name'], 'required'],
			['name', 'trim'],
			['email', 'trim'],
			['email', 'email'],
			['email', 'string', 'max' => 255],
			['id', SocialValidator::class,
				'socialAttribute' => 'social',
				'emailAttribute' => 'email',
				'targetAttribute' => 'socialEntity',
			],
		];
	}

	/**
	 * @return string
	 */
	public function getStatus(): string
	{
		return Social::STATUS_PENDING;
	}

	/**
	 * @return string
	 */
	public function getEmail(): string
	{
		return $this->email;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getSocialId(): string
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getSocial(): string
	{
		return $this->social;
	}

	/**
	 * @return string
	 */
	public function getSocialData(): string
	{
		return json_encode($this->social_data);
	}

	/**
	 * @return int
	 */
	public function getCustomerId()
	{
		return $this->customer_id;
	}

	/**
	 * @return bool
	 */
	public function generateResponse()
	{
		//TODO add try catch
		$this->customer_id = $this->customerRepository->create($this);
		$this->repository->create($this);

		return true;
	}
}