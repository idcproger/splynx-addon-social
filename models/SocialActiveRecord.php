<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class SocialActiveRecord
 *
 * @package app\models
 *
 * @property string $id
 * @property string $customer_id
 * @property string $email
 * @property string $name
 * @property string $social_id
 * @property string $social
 * @property string $social_data
 * @property string $status
 * @property string $created_at
 */
class SocialActiveRecord extends ActiveRecord
{
	public static function tableName()
	{
		return 'social';
	}
}