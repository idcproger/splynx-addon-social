<?php

namespace app\models;

use app\common\Entities\Social;
use app\common\Interfaces\CustomerModelInterface;
use app\common\Interfaces\SocialTableToEditInterface;
use app\common\Validators\SocialValidator;
use yii\base\Model;

/**
 * Class RegistrationConfirm
 *
 * @package app\models
 */
class RegistrationConfirm extends Model implements CustomerModelInterface
{
	public $password;

	public $repassword;

	public $address;

	/** @var  SocialTableToEditInterface */
	public $socialEntity;

	public $id;

	public $social;

	public $name;

	public $email;

	public $login;

	public $customer_id;

	public function rules()
	{
		return [
			[['id', 'social', 'password', 'repassword', 'address', 'name', 'email', 'login'], 'required'],
			['password', 'required'],
			['password', 'string', 'min' => 6],
			['repassword', 'required'],
			['repassword', 'compare', 'compareAttribute' => 'password', 'message' => "Passwords don't match"],
			['address', 'string', 'max' => 255],
			['id', SocialValidator::class,
				'socialAttribute' => 'social',
				'emailAttribute' => 'email',
				'targetAttribute' => 'socialEntity',
			],
			['socialEntity', 'setOtherAttribute'],
		];
	}

	/**
	 * @param $attribute
	 */
	public function setOtherAttribute($attribute)
	{
		/** @var Social $model */
		$model = $this->{$attribute};
		if (empty($model))
		{
			$this->addError($attribute, \Yii::t('app', 'Invalid Params'));

			return;
		}
		\Yii::configure($this, json_decode($model->getSocialData(), true));
	}

	/**
	 * @return string
	 */
	public function getLogin()
	{
		return Social::generateLogin($this->id, $this->social);
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getStatus()
	{
		return Social::STATUS_ACTIVE;
	}

	/**
	 * @return string
	 */
	public function getPassword()
	{
		return $this->password;
	}

	/**
	 * @return string
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * @return string
	 */
	public function getAddress()
	{
		return $this->address;
	}

	public function getCustomerId()
	{
		return $this->socialEntity->getCustomerId();
	}
}