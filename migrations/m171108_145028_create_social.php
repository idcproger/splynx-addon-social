<?php

use yii\db\Migration;
use yii\db\Schema;

class m171108_145028_create_social extends Migration
{
    public function up()
    {
		$this->createTable('social', [
			'id' => Schema::TYPE_PK,
			'customer_id' => Schema::TYPE_INTEGER,
			'email' => Schema::TYPE_STRING . ' NOT NULL',
			'name' => Schema::TYPE_STRING . ' NOT NULL',
			'social_id' => Schema::TYPE_STRING . ' NOT NULL',
			'social_data' => Schema::TYPE_TEXT . ' NOT NULL',
			'social' => Schema::TYPE_STRING . ' NOT NULL',
			'status' => Schema::TYPE_STRING . ' NOT NULL',
			'created_at' => Schema::TYPE_STRING
		], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
    }

    public function down()
    {
		$this->dropTable('social');
    }

}
