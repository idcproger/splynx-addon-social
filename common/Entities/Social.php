<?php

namespace app\common\Entities;

use app\common\Interfaces\SocialTableToEditInterface;
use app\common\Transaction\Transaction;
use app\models\SocialActiveRecord;

/**
 * Class SocialEntity
 *
 * @package app\common\Entities
 */
class Social implements SocialTableToEditInterface
{
	const STATUS_PENDING = 'pending';
	const STATUS_ACTIVE = 'active';

	/** @var SocialActiveRecord */
	protected $activeRecord;

	/** @var Transaction */
	protected $transaction;

	/**
	 * Social constructor.
	 *
	 * @param SocialActiveRecord $activeRecord
	 * @param \app\common\Transaction\Transaction $transaction
	 */
	public function __construct(SocialActiveRecord $activeRecord, Transaction $transaction)
	{
		$this->activeRecord = $activeRecord;
		$this->transaction = $transaction;
	}

	public static function generateLogin(string $social_id, string $social): string
	{
		return md5($social_id . $social);
	}

	/**
	 * @return string|null
	 */
	public function getStatus()
	{
		return $this->activeRecord->status;
	}

	/**
	 * @return string|null
	 */
	public function getName()
	{
		return $this->activeRecord->name;
	}

	/**
	 * @return int
	 */
	public function getCustomerId()
	{
		return $this->activeRecord->customer_id;
	}

	/**
	 * @return null|string
	 */
	public function getSocialId()
	{
		return $this->activeRecord->social_id;
	}

	/**
	 * @return null|string
	 */
	public function getEmail()
	{
		return $this->activeRecord->email;
	}

	/**
	 * @return null|string
	 */
	public function getSocial()
	{
		return $this->activeRecord->social;
	}

	/**
	 * @return null|string
	 */
	public function getSocialData()
	{
		return $this->activeRecord->social_data;
	}

	/**
	 * @param string $id
	 *
	 * @return SocialTableToEditInterface
	 */
	public function setCustomerId(string $id): SocialTableToEditInterface
	{
		return $this->transaction->call(function () use ($id) {
			$this->activeRecord->updateAttributes(['customer_id' => $id]);

			return $this;
		});
	}

	/**
	 * @param string $social_id
	 *
	 * @return SocialTableToEditInterface
	 */
	public function setSocialId(string $social_id): SocialTableToEditInterface
	{
		return $this->transaction->call(function () use ($social_id) {
			$this->activeRecord->updateAttributes(['social_id' => $social_id]);

			return $this;
		});
	}

	/**
	 * @param string $social
	 *
	 * @return SocialTableToEditInterface
	 */
	public function setSocial(string $social): SocialTableToEditInterface
	{
		return $this->transaction->call(function () use ($social) {
			$this->activeRecord->updateAttributes(['social' => $social]);

			return $this;
		});
	}

	/**
	 * @param string $name
	 *
	 * @return SocialTableToEditInterface
	 */
	public function setName(string $name): SocialTableToEditInterface
	{
		return $this->transaction->call(function () use ($name) {
			$this->activeRecord->updateAttributes(['name' => $name]);

			return $this;
		});
	}

	/**
	 * @param string $data
	 *
	 * @return \app\common\Interfaces\SocialTableToEditInterface
	 */
	public function setSocialData(string $data): SocialTableToEditInterface
	{
		return $this->transaction->call(function () use ($data) {
			$this->activeRecord->updateAttributes(['social_data' => $data]);

			return $this;
		});
	}

	/**
	 * @param string $status
	 *
	 * @return SocialTableToEditInterface
	 */
	public function setStatus(string $status): SocialTableToEditInterface
	{
		return $this->transaction->call(function () use ($status) {
			$this->activeRecord->updateAttributes(['status' => $status]);

			return $this;
		});
	}
}