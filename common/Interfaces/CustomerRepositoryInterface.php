<?php

namespace app\common\Interfaces;

/**
 * Interface CustomerRepositoryInterface
 *
 * @package app\common\Interfaces
 */
interface CustomerRepositoryInterface
{
	/**
	 * @param string $email
	 *
	 * @return bool
	 */
	public function existsCustomerByEmail(string $email): bool;

	/**
	 * @param \app\common\Interfaces\SocialInterface $customer
	 *
	 * @return int|null
	 */
	public function create(SocialInterface $customer);

	/**
	 * @param CustomerModelInterface $customer
	 *
	 * @return bool
	 */
	public function update(CustomerModelInterface $customer);

	/**
	 * @param string $email
	 * @param string $text
	 *
	 * @return int
	 */
	public function sendEmail(string $email, string $text);
}