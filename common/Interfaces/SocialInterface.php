<?php

namespace app\common\Interfaces;

/**
 * Interface SocialEntityInterface
 */
interface SocialInterface
{
	/**
	 * @return null|string
	 */
	public function getStatus();

	/**
	 * @return string
	 */
	public function getSocialId();

	/**
	 * @return string
	 */
	public function getSocial();

	/**
	 * @return string
	 */
	public function getSocialData();

	/**
	 * @return string
	 */
	public function getEmail();

	/**
	 * @return string
	 */
	public function getName();

	/**
	 * @return int
	 */
	public function getCustomerId();
}