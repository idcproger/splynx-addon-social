<?php

namespace app\common\Interfaces;

/**
 * Interface SocialTableToEditInterface
 *
 * @package app\common\Interfaces
 */
interface SocialTableToEditInterface extends SocialInterface
{
	/**
	 * @param string $id
	 *
	 * @return SocialTableToEditInterface
	 */
	public function setCustomerId(string $id): SocialTableToEditInterface;

	/**
	 * @param string $social_id
	 *
	 * @return SocialTableToEditInterface
	 */
	public function setSocialId(string $social_id): SocialTableToEditInterface;

	/**
	 * @param string $social
	 *
	 * @return SocialTableToEditInterface
	 */
	public function setSocial(string $social): SocialTableToEditInterface;

	/**
	 * @param object $data
	 *
	 * @return SocialTableToEditInterface
	 */
	public function setSocialData(string $data): SocialTableToEditInterface;

	/**
	 * @param string $status
	 *
	 * @return SocialTableToEditInterface
	 */
	public function setStatus(string $status): SocialTableToEditInterface;

	/**
	 * @param string $name
	 *
	 * @return SocialTableToEditInterface
	 */
	public function setName(string $name): SocialTableToEditInterface;
}