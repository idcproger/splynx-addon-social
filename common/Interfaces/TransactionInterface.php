<?php

namespace app\common\Interfaces;

/**
 * Interface TransactionInterface
 */
interface TransactionInterface
{
	/**
	 * @param callable $callable
	 * @param array ...$args
	 *
	 * @throws \Throwable
	 * @return mixed
	 */
	public function call(callable $callable, ...$args);
}