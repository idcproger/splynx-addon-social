<?php

namespace app\common\Interfaces;

/**
 * Class CustomerModelInterface
 *
 * @package app\common\Interfaces
 */
interface CustomerModelInterface
{
	/**
	 * @return string
	 */
	public function getLogin();

	/**
	 * @return string
	 */
	public function getName();

	/**
	 * @return string
	 */
	public function getStatus();

	/**
	 * @return string
	 */
	public function getPassword();

	/**
	 * @return string
	 */
	public function getEmail();

	/**
	 * @return string
	 */
	public function getAddress();

	/**
	 * @return int
	 */
	public function getCustomerId();
}