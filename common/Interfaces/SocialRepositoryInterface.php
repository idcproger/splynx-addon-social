<?php

namespace app\common\Interfaces;

/**
 * Interface SocialRepositoryInterface
 *
 * @package app\common\Interfaces
 */
interface SocialRepositoryInterface
{
	/**
	 * @param SocialInterface $social
	 *
	 * @return SocialInterface
	 */
	public function create(SocialInterface $social): SocialInterface;

	/**
	 * @param string $id
	 * @param string $social
	 *
	 * @return SocialInterface
	 */
	public function findOne(string $id, string $social): SocialInterface;

	/**
	 * @param string $email
	 *
	 * @return SocialInterface
	 */
	public function findByEmail(string $email): SocialInterface;
}