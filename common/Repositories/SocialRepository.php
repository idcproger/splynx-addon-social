<?php

namespace app\common\Repositories;

use app\common\Entities\Social;
use app\common\Interfaces\SocialInterface;
use app\common\Interfaces\SocialRepositoryInterface;
use app\common\Interfaces\SocialTableToEditInterface;
use app\common\Transaction\Transaction;
use app\models\SocialActiveRecord;
use Yii;

/**
 * Class SocialRepository
 *
 * @package app\models
 */
class SocialRepository implements SocialRepositoryInterface
{
	/** @var SocialActiveRecord */
	public $recordClass;

	/** @var Transaction */
	protected $transaction;

	/**
	 * SocialRepository constructor.
	 *
	 * @param SocialActiveRecord $recordClass
	 * @param \app\common\Transaction\Transaction $transaction
	 * @param array $config
	 */
	public function __construct(SocialActiveRecord $recordClass, Transaction $transaction, array $config = [])
	{
		Yii::configure($this, $config);
		$this->transaction = $transaction;
		$this->recordClass = $recordClass;
	}

	/**
	 * @param SocialInterface $social
	 *
	 * @return SocialInterface
	 */
	public function create(SocialInterface $social): SocialInterface
	{
		return $this->transaction->call(function () use ($social) {
			/**@var SocialActiveRecord $record */
			$record = new $this->recordClass;
			$record->customer_id = $social->getCustomerId();
			$record->social_id = $social->getSocialId();
			$record->name = $social->getName();
			$record->social = $social->getSocial();
			$record->social_data = $social->getSocialData();
			$record->email = $social->getEmail();
			$record->status = Social::STATUS_PENDING;
			//TODO require Carbon on composer
			//$record->created_at = Carbon::now()->toDateTimeString();
			$record->save();

			return $this->transform($record);
		});
	}

	/**
	 * @param string $social_id
	 * @param string $social
	 *
	 * @return SocialInterface
	 */
	public function findOne(string $social_id, string $social): SocialInterface
	{
		/** @var SocialActiveRecord $social */
		$social = $this->recordClass::find()
									->andWhere(['=', 'social_id', $social_id])
									->andWhere(['=', 'social', $social])
									->one();
		if (empty($social))
		{
			/** @var SocialTableToEditInterface $social */
			$social = Yii::$container->get(Social::class);

			return $social;
		}

		return $this->transform($social);
	}

	/**
	 * @param string $email
	 *
	 * @return SocialInterface
	 */
	public function findByEmail(string $email): SocialInterface
	{
		/** @var SocialActiveRecord $social */
		$social = $this->recordClass::find()
									->andWhere(['=', 'email', $email])
									->one();
		if (empty($social))
		{
			/** @var SocialTableToEditInterface $social */
			$social = Yii::$container->get(Social::class);

			return $social;
		}

		return $this->transform($social);
	}

	/**
	 * @param SocialActiveRecord $record
	 *
	 * @return SocialTableToEditInterface
	 */
	public function transform(SocialActiveRecord $record): SocialTableToEditInterface
	{
		/** @var SocialTableToEditInterface $category */
		$category = Yii::$container->get(Social::class, [$record]);

		return $category;
	}
}