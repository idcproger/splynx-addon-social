<?php

namespace app\common\Repositories;

use app\common\Entities\Social;
use app\common\Interfaces\CustomerModelInterface;
use app\common\Interfaces\CustomerRepositoryInterface;
use app\common\Interfaces\SocialInterface;
use yii\base\Exception;

/**
 * Class CustomerRepository
 *
 * @package app\common\Repositories
 */
class CustomerRepository implements CustomerRepositoryInterface
{
	const STATUS_ACTIVE = 'active';

	/** @var  int */
	private $responseCode;

	/** @var \SplynxApi */
	protected $api;

	public static $apiCall = 'admin/customers/customer';

	public static $apiCallMail = 'admin/config/mail';

	/**
	 * CustomerRepository constructor.
	 */
	public function __construct()
	{
		$this->api = new \SplynxApi(\Yii::$app->params['api_domain'], \Yii::$app->params['api_key'],
			\Yii::$app->params['api_secret']);
	}

	/**
	 * @param string $email
	 *
	 * @return bool
	 */
	public function existsCustomerByEmail(string $email): bool
	{
		$params = http_build_query(['email' => $email]);
		$result = $this->api->api_call_get(self::$apiCall . '?' . $params);
		if ($result['result'] == false OR empty($result['response']) OR empty($this->api->response))
		{
			return false;
		}

		return true;
	}

	/**
	 * @param SocialInterface $social
	 *
	 * @return mixed
	 * @throws Exception
	 */
	public function create(SocialInterface $social)
	{
		$result = $this->api->api_call_post(self::$apiCall,
			[
				'login' => Social::generateLogin($social->getSocialId(), $social->getSocial()),
				'name' => $social->getName(),
				//				'password' => $customer->getPassword(),
				'email' => $social->getEmail(),
			]
		);
		$this->responseCode = $this->api->response_code;
		if (empty($result))
		{
			//TODO create custom exception
			throw new Exception(json_encode($this->api->response), $this->responseCode);
		}

		return $this->api->response['id'];
	}

	/**
	 * @param \app\common\Interfaces\CustomerModelInterface $customer
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function update(CustomerModelInterface $customer): bool
	{
		$result = $this->api->api_call_put(self::$apiCall,
			$customer->getCustomerId(),
			[
				'login' => $customer->getLogin(),
				'status' => self::STATUS_ACTIVE,
				'password' => $customer->getPassword(),
				'street_1' => $customer->getAddress(),
			]
		);
		$this->responseCode = $this->api->response_code;
		if (empty($result))
		{
			//TODO create custom exception
			throw new Exception(json_encode($this->api->response), $this->responseCode);
		}

		return true;
	}

	/**
	 * @return int
	 */
	public function getResponseCode()
	{
		return $this->responseCode;
	}

	/**
	 * @param string $email
	 * @param string $text
	 *
	 * @return int
	 * @throws \yii\base\Exception
	 */
	public function sendEmail(string $email, string $text): int
	{
		$result = $this->api->api_call_post(self::$apiCallMail,
			[
				'type' => 'message',
				'recipient' => $email,
				'message' => $text,
			]
		);
		$this->responseCode = $this->api->response_code;
		if (empty($result))
		{
			//TODO create custom exception
			throw new Exception(json_encode($this->api->response), $this->responseCode);
		}

		return $this->api->response['id'];
	}
}