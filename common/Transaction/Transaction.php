<?php

namespace app\common\Transaction;

use app\common\Interfaces\TransactionInterface;
use yii\db\Connection;

/**
 * Class Transaction
 */
class Transaction implements TransactionInterface
{
	/** @var  Connection */
	protected $connection;

	/**
	 * Transaction constructor.
	 *
	 * @param Connection $connection
	 */
	public function __construct(Connection $connection)
	{
		$this->connection = \Yii::$app->db;
	}

	/**
	 * @param callable $callable
	 * @param array ...$args
	 *
	 * @return mixed
	 * @throws \Throwable
	 */
	public function call(callable $callable, ...$args)
	{
		if ($this->connection->transaction && $this->connection->transaction->isActive)
		{
			return call_user_func_array($callable, $args);
		}
		$transaction = $this->connection->beginTransaction();
		try
		{
			$result = call_user_func_array($callable, $args);
			$transaction->commit();

			return $result;
		} catch (\Throwable $exception)
		{
			$transaction->rollBack();
			throw $exception;
		}
	}
}