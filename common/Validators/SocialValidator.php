<?php

namespace app\common\Validators;

use app\common\Entities\Social;
use app\common\Interfaces\SocialInterface;
use app\common\Repositories\SocialRepository;
use Yii;
use yii\validators\Validator;

/**
 * Class CustomerSocialValidator
 *
 * @package app\common\Valodators
 */
class SocialValidator extends Validator
{
	/** @var  string */
	public $socialAttribute;

	/** @var  string */
	public $emailAttribute;

	/** @var  string */
	public $targetAttribute;

	/** @var SocialRepository */
	protected $repository;

	/**
	 * CustomerSocialValidator constructor.
	 *
	 * @param SocialRepository $repository
	 * @param array $config
	 */
	public function __construct(SocialRepository $repository, array $config = [])
	{
		$this->repository = $repository;
		parent::__construct($config);
	}

	public function init()
	{
		parent::init();
		if ($this->message === null)
		{
			$this->message = \Yii::t('app', '{attribute} is invalid.');
		}
	}

	/**
	 * @param \yii\base\Model $model
	 * @param string $attribute
	 */
	public function validateAttribute($model, $attribute)
	{
		/** @var SocialInterface $emailFind */
		$emailFind = $this->repository->findByEmail($model->{$this->emailAttribute});
		if (!empty($emailFind->getStatus()))
		{
			if ($emailFind->getStatus() == Social::STATUS_ACTIVE || $emailFind->getSocialId() != $model->{$attribute})
			{
				$this->addError($model, $this->emailAttribute, Yii::t('app', 'Email does not unique'));

				return;
			}
			if (!empty($this->targetAttribute))
			{
				$model->{$this->targetAttribute} = $emailFind;
			}

			return;
		}
		$social = $this->repository->findOne($model->{$attribute}, $model->{$this->socialAttribute});
		if (!empty($social->getStatus()) && $social->getStatus() != Social::STATUS_PENDING)
		{
			$this->addError($model, $attribute, $this->message);

			return;
		}
		if (!empty($this->targetAttribute))
		{
			$model->{$this->targetAttribute} = $social;
		}
	}
}