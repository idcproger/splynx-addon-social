<?php

namespace app\common\Valodators;

use app\common\Repositories\SocialRepository;
use yii\validators\Validator;

/**
 * Class CustomerSocialValidator
 *
 * @package app\common\Valodators
 */
class CustomerSocialValidator extends Validator
{
	/** @var  string */
	public $socialAttribute;

	/** @var  string */
	public $targetAttribute;

	/** @var SocialRepository */
	protected $repository;

	/**
	 * CustomerSocialValidator constructor.
	 *
	 * @param SocialRepository $repository
	 * @param array $config
	 */
	public function __construct(SocialRepository $repository, array $config = [])
	{
		$this->repository = $repository;
		parent::__construct($config);
	}

	public function init()
	{
		parent::init();
		if ($this->message === null)
		{
			$this->message = \Yii::t('app', '{attribute} is invalid.');
		}
	}

	/**
	 * @param \yii\base\Model $model
	 * @param string $attribute
	 */
	public function validateAttribute($model, $attribute)
	{
		$social = $this->repository->findOne($model->{$attribute}, $model->{$this->socialAttribute});
		if (!empty($this->targetAttribute))
		{
			$model->{$this->targetAttribute} = $social;
		}
	}
}