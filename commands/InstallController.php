<?php

namespace app\commands;

use splynx\helpers\IPHelper;
use yii\console\Controller;

class InstallController extends Controller
{
	public function actionIndex()
	{
		echo 'Start install..' . "\n";

		$baseDir = \Yii::$app->getBasePath();

		$paramsFilePath = $baseDir . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'params.php';

		// Chmod
		$paths = [
			\Yii::$app->getBasePath() . DIRECTORY_SEPARATOR . 'runtime' => '0777',
			\Yii::$app->getBasePath() . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'assets' => '0777',
			\Yii::$app->getBasePath() . DIRECTORY_SEPARATOR . 'yii' => '0755'
		];
		self::chmodFiles($paths);

		// Create table in DB
		\Yii::$app->runAction('migrate/up', [
			'interactive' => '0'
		]);

		// Chmod db file
		$paths = [
			\Yii::$app->getBasePath() . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'data.db' => '0777'
		];
		// Set Splynx dir
		$splynxDir = '/var/www/splynx/';

		// Create API key
		$apiKeyId = (int)exec($splynxDir . 'system/script/addon add-or-get-api-key --title="Splynx Add-on Social"');
		if (!$apiKeyId) {
			exit("Error: Create API key failed!\n");
		}

		// Set permissions
		exec($splynxDir . 'system/script/addon set-api-key-permission --id="' . $apiKeyId . '" --controller="api\admin\administration\Administrators" --action="index" --rule="allow"');
		exec($splynxDir . 'system/script/addon set-api-key-permission --id="' . $apiKeyId . '" --controller="api\admin\customers\Customer" --action="index" --rule="allow"');

		// Get Api Key key and secret
		$result = exec($splynxDir . 'system/script/addon get-api-key-and-secret --id=' . $apiKeyId);
		if (!$result) {
			exit("Error: Get API key anf secret failed!\n");
		}
		list($apiKeyKey, $apiKeySecret) = explode(',', $result);


		// Add IPs to white list
		$ips = array_keys(IPHelper::getIpsArray());
		$baseIP = reset($ips);
		exec($splynxDir . 'system/script/addon api-key-white-list --id=' . $apiKeyId . ' --list="' . implode(',', $ips) . '"');

		// Add module
		$result = exec($splynxDir . 'system/script/addon add-or-get-module --module="splynx_addon_social" --title="Splynx Add-on Social"');

		if (trim($result) != 'splynx_addon_social') {
			exit('Error adding add-on');
		}

		// Add module entry point
		$result = exec($splynxDir . 'system/script/addon add-or-get-entry-point '
			. '--module="splynx_addon_social" --title="Splynx Add-on Social" --root="controllers\admin\CustomersController" '
			. '--url="%2Fsocial" --icon="fa-puzzle-piece" --name="social"');

		if (intval($result) == 0) {
			exit('Error adding add-on entry point');
		}

		// Check if already installed
		if (file_exists($paramsFilePath)) {
			exit('Params file already exists! Terminate!' . "\n");
		}

		// Generate config
		$baseParamsFile = \Yii::$app->getBasePath() . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'params.example.php';
		$params = file_get_contents($baseParamsFile);

		// Set Api host to config
		$params = preg_replace('/(("|\')api_domain("|\')\s*=>\s*)(""|\'\')/', "\\1'http://$baseIP/'", $params);

		// Set Api key to config
		$params = preg_replace('/(("|\')api_key("|\')\s*=>\s*)(""|\'\')/', "\\1'$apiKeyKey'", $params);

		// Set Api secret to config
		$params = preg_replace('/(("|\')api_secret("|\')\s*=>\s*)(""|\'\')/', "\\1'$apiKeySecret'", $params);

		// Set Api portal url to config
		$params = preg_replace('/(("|\')api_portal_url("|\')\s*=>\s*)(""|\'\')/', "\\1'http://$baseIP/portal'", $params);

		// Cookie
		$cookieValidationKey = self::generateRandomString();
		$params = preg_replace('/(("|\')cookieValidationKey("|\')\s*=>\s*)(""|\'\')/', "\\1'$cookieValidationKey'", $params);

		// Save config
		file_put_contents($paramsFilePath, $params);

		// Reload nginx
		if (file_exists('/etc/init.d/nginx')) {
			exec('/etc/init.d/nginx restart  > /dev/null 2>&1 3>/dev/null');
		}

		// Reload apache
		if (file_exists('/etc/init.d/apache2')) {
			exec('/etc/init.d/apache2 restart  > /dev/null 2>&1 3>/dev/null');
		}

		echo 'Installed!' . "\n";
	}

	protected static function generateRandomString()
	{
		if (!extension_loaded('openssl')) {
			throw new \Exception('The OpenSSL PHP extension is required by Yii2.');
		}
		$length = 32;
		$bytes = openssl_random_pseudo_bytes($length);
		return strtr(substr(base64_encode($bytes), 0, $length), '+/=', '_-.');
	}

	protected static function chmodFiles($paths)
	{
		foreach ($paths as $path => $permission) {
			echo "chmod('$path', $permission)...";
			if (is_dir($path) || is_file($path)) {
				try {
					if (chmod($path, octdec($permission))) {
						echo "done.\n";
					};
				} catch (\Exception $e) {
					echo $e->getMessage() . "\n";
				}
			} else {
				echo "file not found.\n";
			}
		}
	}

}
