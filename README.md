Splynx Add-on Social
======================

Splynx Add-on Social based on [Yii2](http://www.yiiframework.com).

INSTALLATION
------------

Install add-on base:
~~~
cd /var/www/splynx/addons/
git clone https://bitbucket.org/splynx/splynx-addon-base.git
cd splynx-addon-base
composer global require "fxp/composer-asset-plugin:^1.2.0"
composer install
composer require yiisoft/yii2-authclient
~~~

Install Splynx Add-on Social:
~~~
cd /var/www/splynx/addons/
git clone git@bitbucket.org:idcproger/splynx-addon-social.git
cd splynx-addon-social
composer install
sudo chmod +x yii
./yii install
~~~

Create symlink:
~~~
ln -s /var/www/splynx/addons/splynx-addon-social/web/ /var/www/splynx/web/social
~~~

Create Nginx config file:
~~~
sudo nano /etc/nginx/sites-available/splynx-social.addons
~~~

with following content:
~~~
location /social
{
        try_files $uri $uri/ /social/index.php?$args;
}
~~~

Restart Nginx:
~~~
sudo service nginx restart
~~~

Some settings avaiable in `/var/www/splynx/addons/splynx-addon-social/config/params.php`.

Some settings avaiable in `/var/www/splynx/addons/splynx-addon-social/config/web.php`.

Change the permissions for your api_key  on http://local/admin

You can then access Splynx Add-On Social in menu "Customers".