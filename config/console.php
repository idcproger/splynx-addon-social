<?php

if (file_exists(__DIR__ . '/params.php')) {
    $params = require(__DIR__ . '/params.php');
} else {
    $params = null;
}

return [
    'id' => 'splynx-addon-social-console',
    'basePath' => dirname(__DIR__),
    'vendorPath' => dirname(__DIR__) . '/../splynx-addon-base/vendor',
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'components' => [
		'db' => [
			'class' => 'yii\db\Connection',
			'dsn' => 'mysql:host=localhost;dbname=splynx',
			'username' => 'root',
			'password' => '123',
			'charset' => 'utf8',
		],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'params' => $params,
];
