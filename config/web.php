<?php

$paramsFile = __DIR__ . DIRECTORY_SEPARATOR . 'params.php';
if (!file_exists($paramsFile)) {
    die('Error: ' . $paramsFile . ' not found! You must copy end edit example file!');
}

$params = require($paramsFile);
$config = parse_ini_file('auth.ini', true);
$config = [
    'id' => 'splynx-addon-social',
    'basePath' => dirname(__DIR__),
    'vendorPath' => dirname(__DIR__) . '/../splynx-addon-base/vendor',
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            'baseUrl' => '/social',
            'enableCookieValidation' => false
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
		'i18n' => [
			'translations' => [
				'*' => [
					'class' => 'yii\i18n\PhpMessageSource',
					'basePath' => '@app/messages', // if advanced application, set @frontend/messages
					'sourceLanguage' => 'en',
					'fileMap' => [
						//'main' => 'main.php',
					],
				],
			],
		],
		'mailer' => [
			'class' => 'yii\swiftmailer\Mailer',
			// send all mails to a file by default. You have to set
			// 'useFileTransport' to false and configure a transport
			// for the mailer to send real emails.
			'useFileTransport' => false,
			'transport' => [
				'class' => 'Swift_SmtpTransport',
				'host' => 'smtp.gmail.com',
				'username' => 'amatoriste@gmail.com',
				'password' => '',
				'port' => '587',
				'encryption' => 'tls',
			],
		],
		 'user' => [
				'identityClass' => 'splynx\models\Customer',
//				'idParam' => 'splynx_customer_id',
//				'loginUrl' => '/social',
				'enableAutoLogin' => true,
			],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
		'db' => require(__DIR__ . '/db.php'),

		'authClientCollection' => [
			'class' => 'yii\authclient\Collection',
			'clients' => [
				'facebook' => [
					'class' => 'yii\authclient\clients\Facebook',
					'clientId' => '2396939930531969',
					'clientSecret' => '3a393f0111c24f5af1e6f75983ce970c',
				],
			/*	'twitter' => [
					'class' => 'yii\authclient\clients\Twitter',
					'consumerKey' => $config['oauth_twitter_key'],
					'consumerSecret' =>  $config['oauth_twitter_secret'],
				],*/
			],
		],
		'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'site/index',
//                '/site' => 'site/index',
				'/site/<alias>' => 'site/<alias>',

				//				'login/<service:google|facebook|etc>' => 'site/login',
                '<alias:registration-confirm>' => 'site/<alias>',
//                '<alias:auth>' => 'site/<alias>',
                '<alias:index>' => 'site/<alias>',
            ],
        ],
        'view' => [
            'class' => 'yii\web\View',
            'defaultExtension' => 'twig',
            'renderers' => [
                'twig' => [
                    'class' => 'yii\twig\ViewRenderer',
                    'cachePath' => '@runtime/Twig/cache',
                    // Array of twig options:
                    'options' => [],
                    'extensions' => [
                        '\splynx\components\twig\TwigActiveForm',
                        '\splynx\components\twig\TwigPjax'
                    ],
                    'globals' => [
                        'Html' => '\yii\helpers\Html',
						'Yii'=> '\yii\BaseYii',
                        'Url' => '\yii\helpers\Url',
                        'GridView' => '\yii\grid\GridView',
                        'DatePicker' => '\yii\jui\DatePicker',
                        'ActiveForm' => '\ yii\widgets\ActiveForm',
                    ],
                    'uses' => [
                        'yii\bootstrap'
                    ],
                ],
            ],
        ],
    ],
    'params' => $params,
/*	'modules' => [
		'user' => [
			'class' => 'dektrium\user\Module',
			'mailer' => [
				'sender'                => 'no-reply@myhost.com', // or ['no-reply@myhost.com' => 'Sender name']
				'welcomeSubject'        => 'Welcome subject',
				'confirmationSubject'   => 'Confirmation subject',
				'reconfirmationSubject' => 'Email change subject',
				'recoverySubject'       => 'Recovery subject',
			],
		],
	],*/
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['components']['view']['renderers']['twig']['options']['debug'] = true;
    $config['components']['view']['renderers']['twig']['options']['auto_reload'] = true;

    $config['components']['view']['renderers']['twig']['extensions'][] = '\Twig_Extension_Debug';
}

return $config;
