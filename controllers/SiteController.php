<?php

namespace app\controllers;

use app\common\Entities\Social;
use app\common\Interfaces\CustomerRepositoryInterface;
use app\common\Interfaces\SocialRepositoryInterface;
use app\common\Repositories\CustomerRepository;
use app\common\Repositories\SocialRepository;
use app\models\AuthModel;
use app\models\RegistrationConfirm;
use Yii;
use yii\authclient\ClientInterface;
use yii\authclient\widgets\AuthChoice;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Cookie;

/**
 * Class SiteController
 *
 * @package app\controllers
 */
class SiteController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'auth' => [
				'class' => 'yii\authclient\AuthAction',
				'successCallback' => [$this, 'onAuthSuccess'],
				'successUrl' => 'registration-confirm',
			],
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['logout'],
				'rules' => [
					[
						'actions' => ['logout'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	/**
	 * @return string
	 */
	public function actionIndex()
	{
		$this->getView()->title = 'Splynx Add-on Social';

		return $this->render('index',
			[
				'portalUrl'=>Yii::$app->params['api_domain'].'portal',
				'facebook' => AuthChoice::widget([
					'baseAuthUrl' => ['site/auth'],
				]),
			]);
	}

	/**
	 * @return array|string|\yii\web\Response
	 * @throws \yii\base\Exception
	 */
	public function actionRegistrationConfirm()
	{
		$this->getView()->title = 'Social Registration Confirm';
		$cookies = Yii::$app->request->cookies;
		if (!$cookies->has('social_id') || !$cookies->has('social'))
		{
			return Yii::$app->controller->redirect(Yii::$app->params['api_domain'] . 'social');
		}
		$social_id = $cookies->get('social_id')->value;
		$social = $cookies->get('social')->value;
		$confirmModel = new RegistrationConfirm();
		if (Yii::$app->request->isPost)
		{
			$confirmModel->setAttributes(['id' => $social_id, 'social' => $social]);
			if (!$confirmModel->load(Yii::$app->request->post()) || !$confirmModel->validate())
			{
				return $this->render('registrationConfirm',
					['model' => $confirmModel,
						'errors' => $confirmModel->getErrors(),
					]);
			}
			/** @var CustomerRepositoryInterface $customerRepository */
			$customerRepository = Yii::$container->get(CustomerRepository::class);
			$customerRepository->update($confirmModel);
			$confirmModel->socialEntity->setStatus(Social::STATUS_ACTIVE);
			$customerRepository->sendEmail($confirmModel->getEmail(),
				$confirmModel->getName() . ', congratulations! You login - ' . $confirmModel->getLogin());

			return $this->redirect(Yii::$app->params['api_domain'] . 'portal');
		}
		/** @var SocialRepositoryInterface $repository */
		$repository = Yii::$container->get(SocialRepository::class);
		$socialEntity = $repository->findOne($social_id, $social);
		//TODO Facebook only. Need  create rules for  all socials
		if ($socialEntity->getStatus() != Social::STATUS_PENDING)
		{
			return $this->redirect('error');
			//			throw new Exception('Invalid social');
		}
		$socialData = json_decode($socialEntity->getSocialData(), true);
		$confirmModel->setAttributes(array_merge($socialData,
			['social' => $social, 'login' => Social::generateLogin($social_id, $social)]));

		return $this->render('registrationConfirm', ['model' => $confirmModel]);
	}

	/**
	 * @param ClientInterface $client
	 *
	 * @return string|void
	 * @throws \yii\base\Exception
	 */
	public function onAuthSuccess(ClientInterface $client)
	{
		/** @var CustomerRepositoryInterface $customerRepository */
		//		$customerRepository = Yii::$container->get(CustomerRepository::class);
		//TODO check  exist customer in Splynx by email. Dont work response returned all customers. ??
		//			if ($customerRepository->existsCustomerByEmail($attributes['email']))
		//		{
		//			throw new Exception('Email does not unique');
		//			//TODO need create custom exception
		//		}
		$attributes = $client->getUserAttributes();
		/** @var AuthModel $authModel */
		$authModel = Yii::$container->get(AuthModel::class);
		//TODO only facebook, if need morem must create social mock models
		$authModel->setAttributes(array_merge(['social' => $client->getId(), 'social_data' => $attributes],
			$attributes));
		if (!$authModel->validate())
		{
			throw new Exception($authModel->getErrors());
			//TODO need create custom exception
		}
		if ($authModel->socialEntity->getStatus() != Social::STATUS_PENDING && !$authModel->generateResponse())
		{
			throw new Exception($authModel->getErrors());
			//TODO need create custom exception
		}
		Yii::$app->response->cookies->add(new Cookie(['name' => 'social_id', 'value' => $authModel->id]));
		Yii::$app->response->cookies->add(new Cookie(['name' => 'social', 'value' => $authModel->social]));

		return;
	}
}
